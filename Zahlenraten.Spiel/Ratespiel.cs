﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zahlenraten.Spiel
{
    public class Ratespiel
    {
        private int _zuErratendeZahl;
        private int _rateVersuche = 0;
        private IRatezahlGenerator _generator;

        public Ratespiel()
            : this(new RatezahlGeneratorRandom())
        {
        }

        public Ratespiel(IRatezahlGenerator generator)
        {
            _generator = generator;
            NeuesSpiel();
        }

        public int RateVersuche
        {
            get
            {
                return _rateVersuche;
            }
        }

        public void NeuesSpiel()
        {
            _zuErratendeZahl = _generator.NeueRatezahl();
            _rateVersuche = 0;
        }

        public RateErgebnis TesteZahl(int zahl)
        {
            _rateVersuche += 1;
            if (zahl > _zuErratendeZahl)
                return RateErgebnis.ZuGross;
            if (zahl < _zuErratendeZahl)
                return RateErgebnis.ZuKlein;
            return RateErgebnis.Erraten;
        }
    }
}
