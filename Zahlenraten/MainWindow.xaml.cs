﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Zahlenraten.Spiel;

namespace Zahlenraten
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Ratespiel _spiel = new Ratespiel();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (!Ratezahl.Value.HasValue)
            {
                MessageBox.Show("Bitte geben Sie eine Zahl ein!");
                return;
            }

            var ratezahl = Ratezahl.Value.Value;
            var rateErgebnis = _spiel.TesteZahl(ratezahl);
            AnzahlVersuche.Text = _spiel.RateVersuche.ToString();

            if (rateErgebnis == RateErgebnis.ZuKlein)
            {
                MessageBox.Show("Zu klein!");
            }
            else if (rateErgebnis == RateErgebnis.ZuGross)
            {
                MessageBox.Show("Zu groß!");
            }
            else
            {
                var meldung = Jeffijoe.MessageFormat.MessageFormatter.Format(
                    @"Richtig geraten nach {versuche, plural, one {einem Versuch} other {# Versuchen}}! Wollen Sie ein neues Spiel starten?",
                    new { versuche = _spiel.RateVersuche });
                var ergebnis = MessageBox.Show(meldung, "Erfolg", MessageBoxButton.YesNo);
                if (ergebnis == MessageBoxResult.Yes)
                {
                    NeuesSpiel();
                }
                else if (ergebnis == MessageBoxResult.No)
                {
                    Close();
                }
            }
        }

        private void NeuesSpiel()
        {
            _spiel.NeuesSpiel();
            AnzahlVersuche.Text = _spiel.RateVersuche.ToString();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            NeuesSpiel();
            Ratezahl.Focus();
        }

        private void SpielNeustarten_Click(object sender, RoutedEventArgs e)
        {
            var ergebnis = MessageBox.Show("Wollen Sie wirklich ein neues Spiel starten?", "Neues Spiel", MessageBoxButton.OKCancel);
            if (ergebnis == MessageBoxResult.OK)
            {
                NeuesSpiel();
            }
        }
    }
}
