﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Zahlenraten.Spiel;

namespace Zahlenraten.Test
{
    class RatezahlGeneratorFesterWert : IRatezahlGenerator
    {
        private readonly int _wert;

        public RatezahlGeneratorFesterWert(int wert)
        {
            _wert = wert;
        }

        public int NeueRatezahl()
        {
            return _wert;
        }
    }
}
