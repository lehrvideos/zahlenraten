﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zahlenraten.Spiel
{
    public enum RateErgebnis
    {
        Erraten,
        ZuKlein,
        ZuGross
    }
}
