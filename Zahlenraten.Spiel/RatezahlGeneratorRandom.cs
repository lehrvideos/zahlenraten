﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zahlenraten.Spiel
{
    public class RatezahlGeneratorRandom : IRatezahlGenerator
    {
        private Random _generator = new Random();

        public int NeueRatezahl()
        {
            return _generator.Next(1, 21);
        }
    }
}
