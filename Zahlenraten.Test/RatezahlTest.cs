﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Zahlenraten.Spiel;

namespace Zahlenraten.Test
{
    [TestClass]
    public class RatezahlTest
    {
        [TestMethod]
        public void TestInit()
        {
            var instance = new Ratespiel();
            Assert.AreEqual(0, instance.RateVersuche);
        }

        [TestMethod]
        public void TestVergleiche()
        {
            var instance = new Ratespiel(new RatezahlGeneratorFesterWert(10));
            Assert.AreEqual(RateErgebnis.Erraten, instance.TesteZahl(10));
            Assert.AreEqual(RateErgebnis.ZuKlein, instance.TesteZahl(9));
            Assert.AreEqual(RateErgebnis.ZuGross, instance.TesteZahl(11));
            Assert.AreEqual(3, instance.RateVersuche);
        }
    }
}
